module.exports = {
    rgbToHex: function (red, green, blue) {
        return "#" + componentToHex(red) + componentToHex(green) + componentToHex(blue);
    },
    hexToRgb: function(hex) {
        var result = /^#?([a-f\d]{2})([a-f\d]{2})([a-f\d]{2})$/i.exec(hex);
        return result ? {
          red: parseInt(result[1], 16),
          green: parseInt(result[2], 16),
          blue: parseInt(result[3], 16)
        } : null;
    }
};

function componentToHex(c) {
    var hex = parseInt(c, 10);
    hex = hex.toString(16);
    return hex.length == 1 ? "0" + hex : hex;
};
