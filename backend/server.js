const express = require('express')
const converter = require('./converter.js')
const bodyParser = require('body-parser')
const app = express()
const port = 3000

app.get('/', (req, res) => res.send('Got your message'));

app.get('/data', (req, res) => res.send('Here is your data: 123'));

app.get('/name-query', (req, res) => {
  const fname = req.query.fname;
  const lname = req.query.lname;
  res.send("Your name is " + fname + " " + lname);
})

app.get('/rgb-to-hex', (req, res) => {
  const red = req.query.red;
  const green = req.query.green;
  const blue = req.query.blue;
  hex = converter.rgbToHex(red, green, blue);
  res.send("HEX value: " + hex);
})

var jsonParser = bodyParser.json()

app.post('/add', jsonParser, (req, res) => {
  res.send("Client: " + req.body.client_name + " sum is " + (req.body.num1 + req.body.num2));
});

app.post('/hex-to-rgb', jsonParser, (req, res) => {
  const hex = req.body.hex;
  rgb = converter.hexToRgb(hex);
  res.send(rgb);
});

app.listen(port, () => console.log(`Listening on port ${port}`));
