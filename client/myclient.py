import requests

header = 'Content-Type'

class Client:
    def __init__(self, ploads, endpoints, method):
        self.ploads = ploads
        self.endpoints = endpoints
        self.method = method

task1 = Client(None, 'http://localhost:3000/', 'get')
task2 = Client(None, 'http://localhost:3000/data', 'get')
task3 = Client({'fname':"Anton",'lname':"Ivanov"}, 'http://localhost:3000/name-query', 'get')
task4 = Client({'red':107, 'green':215, 'blue':69}, 'http://localhost:3000/rgb-to-hex', 'get')
task5 = Client({'num1':0.5, 'num2':30.5, 'client_name':"John Martinez"}, 'http://localhost:3000/add', 'post')
task6 = Client({'hex':"#6bd745"}, 'http://localhost:3000/hex-to-rgb', 'post')

tasks = [task1, task2, task3, task4, task5, task6]

i = 0
n = 1

for i in tasks:
    if i.method == 'post':
        r = requests.post(i.endpoints, json = i.ploads)
    else:
        r = requests.get(i.endpoints, params = i.ploads)
    r.headers[header]
    print('Task ' + str(n) + ': ' + r.text + '\n')
    n += 1
